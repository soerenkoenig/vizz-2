import { useCallback, useEffect, useState } from "react";
import { BufferGeometry, Material, PointsMaterial } from "three";
import { XYZLoader } from "three/examples/jsm/loaders/XYZLoader";
import { PLYLoader } from "three/examples/jsm/loaders/PLYLoader";
import { PCDLoader } from "three/examples/jsm/loaders/PCDLoader";
import { XYZRawLoader } from "../XYZRawLoader"
import { XYZRGBRawLoader } from "../XYZRGBRawLoader"
import { useLoader } from "@react-three/fiber";

export interface PointCloudProps {
  url: string;
  extension: string;
}

const PointCloud = (props: PointCloudProps) => {
  const [geometry, setGeometry] = useState<BufferGeometry>();
  const [material, setMaterial] = useState<Material | Material[]>();
  let loader;

  if (props.extension === "ply") {
    loader = PLYLoader;
  } else if (props.extension === "pcd") {
    loader = PCDLoader;
  } else if (props.extension === "rawxyz") {
    loader = XYZRawLoader;
  } else if (props.extension === "rawxyzrgb") {
    loader = XYZRGBRawLoader;
  }
  else{
    loader = XYZLoader;
  }
  const data = useLoader(loader, props.url, undefined, (xhr) => {
    if(xhr.total !== 0)
     console.log(xhr.loaded / xhr.total);
  });

  useEffect(() => {
    if (data instanceof BufferGeometry) {
      setGeometry(data);
      setMaterial(
        new PointsMaterial({
          size: 0.001,
          sizeAttenuation: false,
          vertexColors: data.hasAttribute("color"),
          color: "black",
        })
      );
    } else {
      setGeometry(data.geometry);
      setMaterial(data.material);
    }
  }, [data]);

  if (!geometry || !material) return null;

  return <points geometry={geometry} material={material} />;
};

export const PointClouds = (props: { files: File[] }) => {
  const [clouds, setClouds] = useState<PointCloudProps[]>([]);

  const loadFile = useCallback((file: File) => {
    const extension = file.name.split(".").pop()?.toLocaleLowerCase()!;

    const reader = new FileReader();
    reader.onload = () => {
      const cloud: PointCloudProps = {
        url: reader.result as string,
        extension,
      };
      setClouds((clouds) => [...clouds, cloud]);
    };

    reader.readAsDataURL(file);
  }, []);
  useEffect(() => {
    props.files.forEach((file) => loadFile(file));
  }, [props.files, loadFile]);

  const pcs = clouds.map((cloud, index) => (
    <PointCloud key={index} {...cloud} />
  ));
  return <group>{pcs}</group>;
};

export default PointCloud;
