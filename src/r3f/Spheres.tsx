import { Detailed } from "@react-three/drei";
import { SphereGeometry, Vector3, ColorRepresentation } from "three";

const distances = [15, 30, 45, 60, 75, 90, 105, 120, 135, 150];

const sphereGeometries = distances.map(
  (distances) => new SphereGeometry(1, distances / 3, distances / 3)
);

interface LoDSphereProps {
  center: Vector3;
  radius: number;
  color: ColorRepresentation;
}

const LoDSphere = (props: LoDSphereProps) => {
  const meshes = sphereGeometries.map((geom, index) => (
    <mesh
      position={props.center}
      scale={[props.radius, props.radius, props.radius]}
      key={index}
      geometry={geom}
    >
      <meshStandardMaterial color={props.color} />
    </mesh>
  ));
  return <Detailed distances={distances}>{meshes}</Detailed>;
};

interface SpheresProps {
  color?: number;
  primitives: Array<Array<number>>;
  colors?: Array<number>;
}

const Spheres = (props: SpheresProps) => {
  const spheres = props.primitives.map((cylinder, index) => {
    const color = props.colors
      ? props.colors[index]
      : props.color ?? "0xff0000";
    const radius = cylinder[3];
    const center = new Vector3(cylinder[0], cylinder[1], cylinder[2]);
    return (
      <LoDSphere key={index} center={center} radius={radius} color={color} />
    );
  });
  return <>{spheres}</>;
};

export default Spheres;
