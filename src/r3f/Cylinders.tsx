import { Detailed } from "@react-three/drei";
import {
  DoubleSide,
  CylinderGeometry,
  Vector3,
  ColorRepresentation,
  Quaternion,
  Euler,
} from "three";

const distances = [15, 30, 45, 60, 75, 90, 105, 120, 135, 150];
const cylinderGeometries = distances.map(
  (distances) => new CylinderGeometry(1.0, 1.0, 1.0, distances / 3, 1, true)
);

interface LoDCylinderProps {
  start: Vector3;
  end: Vector3;
  radius: number;
  color: ColorRepresentation;
}

const LoDCylinder = (props: LoDCylinderProps) => {
  let delta = props.end.clone().sub(props.start);
  const height = delta.length();
  const position = props.start.clone().add(delta.clone().multiplyScalar(0.5));
  delta = delta.normalize();

  const yAxis = new Vector3(0, 1, 0);
  const axis = yAxis.clone().cross(delta).normalize();
  const angle = yAxis.angleTo(delta);
  const meshes = cylinderGeometries.map((geom, index) => (
    <mesh
      position={position}
      scale={[props.radius, height, props.radius]}
      rotation={new Euler().setFromQuaternion(
        new Quaternion().setFromAxisAngle(axis, angle)
      )}
      key={index}
      geometry={geom}
    >
      <meshStandardMaterial color={props.color} side={DoubleSide} />
    </mesh>
  ));
  return <Detailed distances={distances}>{meshes}</Detailed>;
};

interface CylindersProps {
  color?: number;
  primitives: Array<Array<number>>;
  colors?: Array<number>;
}

const Cylinders = (props: CylindersProps) => {
  const cylinders = props.primitives.map((cylinder, index) => {
    const color = props.colors
      ? props.colors[index]
      : props.color ?? "0xff0000";
    const radius = cylinder[6];
    const start = new Vector3(cylinder[0], cylinder[1], cylinder[2]);
    const end = new Vector3(cylinder[3], cylinder[4], cylinder[5]);
    return (
      <LoDCylinder
        key={index}
        start={start}
        end={end}
        radius={radius}
        color={color}
      />
    );
  });

  return <>{cylinders}</>;
};
export default Cylinders;
