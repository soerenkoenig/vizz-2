import { TransformControls } from "@react-three/drei";
import { MeshProps } from "@react-three/fiber";
//import { useHover } from "../hooks/useHover";
//import { useSelect } from "../hooks/useSelect";

const Bounds = (props: MeshProps) => {
  const { position, ...rest } = props;
  //const [bindHover, hovered] = useHover();
  //let [bindSelect, selected] = useSelect();
  //  <mesh {...props} {...bindHover} >

  return (
    <TransformControls
      position={position}
      space="world"
      mode="translate"
      showX={true}
      showY={true}
      showZ={true}
      translationSnap={0.1}
    >
      <mesh {...rest}>
        <boxGeometry args={[1, 1, 1]} />
        <meshStandardMaterial color={"#ff0000"} />
      </mesh>
    </TransformControls>
  );
};
export default Bounds;
