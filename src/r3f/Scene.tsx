import { Grid } from "@react-three/drei";
//import Bounds from "./Bounds";

const Scene = () => {
  return (
    <>
      <pointLight position={[10, 10, 10]} />
      <Grid
        args={[32, 32]}
        position={[0, -0.01, 0]}
        infiniteGrid
        fadeDistance={16}
        fadeStrength={0.8}
        cellSize={0.1}
        sectionSize={1}
        sectionColor={0x111111}
        followCamera={true}
      />
    </>
  );
};

export default Scene;
