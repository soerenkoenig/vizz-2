import { useCallback, useEffect, useRef } from "react";

interface DragAndDropProps {
  handleDrop: (fileList: FileList) => any;
  children: React.ReactElement | React.ReactElement[];
}

const DragAndDrop = (props: DragAndDropProps) => {
  const dropRef = useRef<HTMLDivElement>(null);

  const handleDrag = useCallback((e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  }, []);

  const handleDragIn = useCallback((e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  }, []);

  const handleDragOut = useCallback((e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  }, []);

  const handleDrop = useCallback(
    (e: DragEvent) => {
      e.preventDefault();
      e.stopPropagation();
      if (e.dataTransfer) {
        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
          props.handleDrop(e.dataTransfer.files);
          e.dataTransfer.clearData();
        }
      }
    },
    [props]
  );

  useEffect(() => {
    const div = dropRef.current;
    if (div) {
      div.addEventListener("dragenter", handleDragIn);
      div.addEventListener("dragleave", handleDragOut);
      div.addEventListener("dragover", handleDrag);
      div.addEventListener("drop", handleDrop);
    }

    // 👇️ remove the event listener when component unmounts
    return () => {
      if (div) {
        div.removeEventListener("dragenter", handleDragIn);
        div.removeEventListener("dragleave", handleDragOut);
        div.removeEventListener("dragover", handleDrag);
        div.removeEventListener("drop", handleDrop);
      }
    };
  }, [handleDrag, handleDragIn, handleDragOut, handleDrop]);

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
      ref={dropRef}
    >
      {props.children}
    </div>
  );
};
export default DragAndDrop;
