import {
    BufferGeometry,
    FileLoader,
    Float32BufferAttribute,
    Loader
} from 'three';

class XYZRGBRawLoader extends Loader {

    load(url:string, 
        onLoad:(geometry: BufferGeometry) => void
    , onProgress?:(event: ProgressEvent) => void, onError?: (event: ErrorEvent) => void):void {

        const scope = this;

        const loader = new FileLoader(this.manager);
        loader.setPath(this.path);
        loader.setResponseType('arraybuffer');
        loader.setRequestHeader(this.requestHeader);
        loader.setWithCredentials(this.withCredentials);
        loader.load(url, function (text:string | ArrayBuffer) {

            try {
                if (typeof text === "string")
                {
                  onLoad(scope.parse( new TextEncoder().encode(text)));
                }else
                {
                  onLoad(scope.parse(text));
                }

            } catch (e) {

                if (onError) {

                    onError(e as ErrorEvent);

                } else {

                    console.error(e);

                }

                scope.manager.itemError(url);

            }

        }, onProgress, onError);

    }

    parse(data: ArrayBuffer ): BufferGeometry {
        var dataview = new DataView(data);
       
        let vertices = new Array<number>();    
        let colors = new Array<number>();

        for(let j = 0; j < dataview.byteLength;)
        {  
            const x = dataview.getFloat64(j, true);
            j += 8;
            const y = dataview.getFloat64(j, true);
            j += 8;
            const z = dataview.getFloat64(j, true);
            j += 8;
            const r = dataview.getUint16(j, true)/ 65535.0;
            j += 2;
            const g = dataview.getUint16(j, true)/ 65535.0;
            j += 2;
            const b = dataview.getUint16(j, true) / 65535.0;
            j += 2;
            vertices.push(x,y,z);
            colors.push(r,g,b);
        }
       
        const geometry = new BufferGeometry();
        geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
        geometry.setAttribute('color', new Float32BufferAttribute(colors, 3));
        return geometry;

    }

}

export { XYZRGBRawLoader };