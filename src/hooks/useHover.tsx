import { ThreeEvent } from "@react-three/fiber";
import { useCallback, useState } from "react";

export const useHover = (): [
  {
    onPointerOver: (event: ThreeEvent<PointerEvent>) => void;
    onPointerOut: (event: ThreeEvent<PointerEvent>) => void;
  },
  boolean
] => {
  const [hovered, setHover] = useState(false);

  const hover = useCallback((event: ThreeEvent<PointerEvent>) => {
    event.stopPropagation();
    setHover(true);
  }, []);

  const unHover = useCallback(() => setHover(false), []);

  return [{ onPointerOver: hover, onPointerOut: unHover }, hovered];
};
