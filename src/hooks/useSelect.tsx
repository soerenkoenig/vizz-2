import { ThreeEvent } from "@react-three/fiber";
import { useCallback, useState } from "react";

export const useSelect = (): [
  { onClick: (event: ThreeEvent<MouseEvent>) => void },
  boolean
] => {
  const [clicked, click] = useState(false);

  const handleClick = useCallback((event: ThreeEvent<MouseEvent>) => {
    click((c) => !c);
  }, []);

  return [{ onClick: handleClick }, clicked];
};
