import {
    BufferGeometry,
    FileLoader,
    Float32BufferAttribute,
    Loader
} from 'three';

class XYZRawLoader extends Loader {

    load(url:string, 
        onLoad:(geometry: BufferGeometry) => void
    , onProgress?:(event: ProgressEvent) => void, onError?: (event: ErrorEvent) => void):void {

        const scope = this;

        const loader = new FileLoader(this.manager);
        loader.setPath(this.path);
        loader.setResponseType('arraybuffer');
        loader.setRequestHeader(this.requestHeader);
        loader.setWithCredentials(this.withCredentials);
        loader.load(url, function (text:string | ArrayBuffer) {

            try {
                if (typeof text === "string")
                {
                  onLoad(scope.parse( new TextEncoder().encode(text)));
                }else
                {
                  onLoad(scope.parse(text));
                }

            } catch (e) {

                if (onError) {

                    onError(e as ErrorEvent);

                } else {

                    console.error(e);

                }

                scope.manager.itemError(url);

            }

        }, onProgress, onError);

    }

    parse(data: ArrayBuffer ): BufferGeometry {
        var dataview = new DataView(data);
        const n = dataview.byteLength / 8.0;
        let vertices = new Array<number>();

        for(let i = 0; i < n; i++)
        {
            vertices.push(dataview.getFloat64(i*8,true));
        }
       
        

        const geometry = new BufferGeometry();
        geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
        return geometry;

    }

}

export { XYZRawLoader };