import { OrbitControls } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { useCallback, useState } from "react";
import "./App.css";
import DragAndDrop from "./ui/DragAndDrop";

import Scene from "./r3f/Scene";
import { PointClouds } from "./r3f/PointCloud";

const App = () => {
  const [files, setFiles] = useState<File[]>([]);

  const loadFile = useCallback((file: File) => {
    setFiles((files) => [...files, file]);
  }, []);

  const onDrop = useCallback(
    (files: FileList) => {
      Array.from(files).forEach((file) => loadFile(file));
    },
    [loadFile]
  );

  return (
    <div className="App">
      <DragAndDrop handleDrop={onDrop}>
        <Canvas camera={{ fov: 75, near: 0.1, far: 8000, position: [5, 5, 5] }}>
          <color attach="background" args={["rgb(163, 194, 194)"]} />
          <Scene />

          <PointClouds files={files} />
          <OrbitControls enableDamping={false} makeDefault />
        </Canvas>
      </DragAndDrop>
    </div>
  );
};

export default App;
